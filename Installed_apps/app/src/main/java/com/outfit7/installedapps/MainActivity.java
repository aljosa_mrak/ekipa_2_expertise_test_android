package com.outfit7.installedapps;

import android.app.ListActivity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create a new list adapter
        List<ApplicationInfo> data = getInstalledApps("outfit7");
        final AppArrayAdapter adapter = new AppArrayAdapter(this, data);

        // Bind to our new adapter.
        setListAdapter(adapter);
        ((ListView)findViewById(android.R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.selectedItem(position);
                adapter.notifyDataSetChanged();
            }
        });
    }

    public ArrayList<ApplicationInfo> getInstalledApps(String match) {
        match = match.toLowerCase();
        final PackageManager pm = getPackageManager();

        // Get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        // Select only those whose package name contains 'match'
        ArrayList<ApplicationInfo> result = new ArrayList<>(packages.size());
        for (ApplicationInfo info : packages) {
            if (info.packageName.toLowerCase().contains(match)) {
                result.add(info);
            }
        }

        // Sort result
        Collections.sort(result, new Comparator<ApplicationInfo>() {
            @Override
            public int compare(ApplicationInfo ai1, ApplicationInfo ai2) {
                return ai1.loadLabel(pm).toString().compareTo(ai2.loadLabel(pm).toString());
            }
        });

        return result;
    }
}
