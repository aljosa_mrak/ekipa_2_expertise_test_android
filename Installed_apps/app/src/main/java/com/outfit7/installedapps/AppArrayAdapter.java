package com.outfit7.installedapps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AppArrayAdapter extends ArrayAdapter<ApplicationInfo> {

    private final ArrayList<ApplicationInfo> data;
    private int selectedPosition = -1;

    AppArrayAdapter(Context context, List<ApplicationInfo> data) {
        super(context, 0, data);
        this.data = new ArrayList<>(data);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final ApplicationInfo appData = data.get(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).
                    inflate(R.layout.list_item, parent, false);
        }

        // Lookup view for data population
        ImageView icon = convertView.findViewById(R.id.icon);
        TextView name = convertView.findViewById(R.id.name);

        // Populate the data
        icon.setImageDrawable(appData.loadIcon(getContext().getPackageManager()));
        name.setText(appData.loadLabel(getContext().getPackageManager()));

        // If selected show additional data
        if(this.selectedPosition != position) {
            convertView.findViewById(R.id.item_additional_info).setVisibility(View.GONE);
        } else {
            convertView.findViewById(R.id.item_additional_info).setVisibility(View.VISIBLE);

            // Lookup view for data population
            TextView packageName = convertView.findViewById(R.id.package_name);
            TextView versionCode = convertView.findViewById(R.id.version_code);
            TextView versionName = convertView.findViewById(R.id.version_name);
            Button button = convertView.findViewById(R.id.start_button);

            // Populate the data
            packageName.setText(appData.packageName);
            try {
                PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(appData.packageName, 0);
                versionCode.setText(String.valueOf(packageInfo.versionCode));
                versionName.setText(packageInfo.versionName);

                // Add click listener to launch the selected app
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = getContext().getPackageManager().getLaunchIntentForPackage(appData.packageName);
                        if (intent != null) {
                            getContext().startActivity(intent);
                        } else {
                            Toast.makeText(getContext(), "Unable to open app", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("AppArrayAdapter", e.getMessage(), e);
                versionCode.setText("-1");
                versionName.setText("-1");
            }
        }

        // Return the completed view to render on screen
        return convertView;
    }

    void selectedItem(int position) {
        this.selectedPosition = position; //selectedPosition must be a global variable
    }
}
