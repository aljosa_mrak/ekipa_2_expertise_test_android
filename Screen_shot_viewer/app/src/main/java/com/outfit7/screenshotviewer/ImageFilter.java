package com.outfit7.screenshotviewer;

import java.io.File;
import java.io.FilenameFilter;


class ImageFilter implements FilenameFilter{

    // Image extensions
    private String[] extension = {".jpg", ".jpeg", ".png", ".bmp"};

    @Override
    public boolean accept(File file, String name) {
        // Check if known extension
        for (String ext : extension) {
            if (name.toLowerCase().endsWith(ext)) {
                return true;
            }
        }
        return false;
    }
}
