package com.outfit7.screenshotviewer;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 12587;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check and ask for storage permissions
        checkPermission();
    }

    private void checkPermission(){
        // Check storage permissions
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // Ask for permissions
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
        } else {
            setGridView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If permissions were granted
        if (requestCode == REQUEST_CODE && (grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            setGridView();
        } else {
            Toast.makeText(this, "Permissions denied. Unable to proceed", Toast.LENGTH_LONG).show();
        }
    }

    private void setGridView() {
        // Create a new grid adapter
        final GridViewAdapter gridAdapter = new GridViewAdapter(this, getData());

        // Bind to our new adapter.
        GridView gridView = findViewById(R.id.gridView);
        gridView.setAdapter(gridAdapter);

        // Add click listener for sending email
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent intent = new Intent(android.content.Intent.ACTION_SENDTO);
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { "" });
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Bug report");
                intent.putExtra(Intent.EXTRA_TEXT, generateMessage());

                Uri uri = Uri.parse("file://" + gridAdapter.getPath(position));
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                intent.setType("application/image");

                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getBaseContext(), "No Application found to sent email", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private String generateMessage() {
        // Build bug report message with device specs
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return String.format(Locale.ENGLISH,
                "Manufacturer:\t%s\n" +
                       "Model:\t\t\t%s\n" +
                       "Screen size:\t%.2fx%.2f inch\n" +
                       "Screen size:\t%dx%d px\n" +
                       "Screen size:\t%.2fx%.2f dp\n" +
                       "Screen density:\t%.2f (%d dpi)\n",
                Build.MANUFACTURER,
                Build.MODEL,
                metrics.widthPixels / metrics.xdpi,
                metrics.heightPixels / metrics.ydpi,
                metrics.widthPixels,
                metrics.heightPixels,
                metrics.widthPixels / (metrics.xdpi / 160.0),
                metrics.heightPixels / (metrics.ydpi / 160.0),
                metrics.density,
                metrics.densityDpi);
    }

    private ArrayList<String> getData() {
        // Get all pictures on device
        File pix = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File pix1 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File screenshots = new File(pix, "Screenshots");
        File pictures = new File(pix1, "Camera");

        // Keep only pictures from screenshots directory
        ArrayList<String> images = new ArrayList<>();
        if (screenshots.listFiles() != null) {
            for (File f : screenshots.listFiles(new ImageFilter())) {
                images.add(f.getAbsolutePath());
            }
        }

        // Keep only pictures from all pictures
        if (pictures.listFiles() != null) {
            for (File f : pictures.listFiles(new ImageFilter())) {
                images.add(f.getAbsolutePath());
            }
        }

        return images;
    }
}