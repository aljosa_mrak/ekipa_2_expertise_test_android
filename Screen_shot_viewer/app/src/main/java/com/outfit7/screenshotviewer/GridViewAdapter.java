package com.outfit7.screenshotviewer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class GridViewAdapter extends ArrayAdapter<String> {

	private ArrayList<String> imagePaths = new ArrayList<>();
    private ImageLoader imageLoader;

    GridViewAdapter(Context context, ArrayList<String> data) {
		super(context, -1, data);
		this.imagePaths = data;
        this.imageLoader = new ImageLoader();
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Check if an existing view is being reused, otherwise inflate the view
        ImageView imageView;
		if (convertView == null) {
            imageView = (ImageView) LayoutInflater.from(getContext()).
                    inflate(R.layout.grid_item_layout, parent, false);
		} else {
			imageView = (ImageView) convertView;
		}

        // Asynchronous image loading
        imageLoader.displayImage(imagePaths.get(position), imageView);

		return imageView;
	}

    String getPath(int position) {
        return imagePaths.get(position);
    }
}