package com.outfit7.screenshotviewer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedHashMap;


class ImageLoader {

    // Cache capacity
    private static final int HARD_CACHE_CAPACITY = 50;

    // Hard cache, with a fixed maximum capacity and a life duration
    private final HashMap<String, Bitmap> bitmapCache =
            new LinkedHashMap<>(HARD_CACHE_CAPACITY / 2, 0.75f, true);


    void displayImage(String path, ImageView imageView) {
        // Check if image is cashed
        Bitmap bitmap = getBitmapFromCache(path);

        if (bitmap == null) {
            // Load image
            forceLoad(path, imageView);
        } else {
            // Image cached -> not need for loading it
            cancelPotentialDownload(path, imageView);
            imageView.setImageBitmap(bitmap);
        }
    }

    private void forceLoad(String path, ImageView imageView) {
        if (path == null) {
            imageView.setImageDrawable(null);
            return;
        }

        // Asynchronously load image
        if (cancelPotentialDownload(path, imageView)) {
            BitmapDownloaderTask task = new BitmapDownloaderTask(imageView);
            DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
            imageView.setImageDrawable(downloadedDrawable);
            task.execute(path);
        }
    }

    /**
     * Returns true if the current download has been canceled or if there was no download in
     * progress on this image view.
     * Returns false if the download in progress deals with the same path. The download is not
     * stopped in that case.
     */
    private static boolean cancelPotentialDownload(String path, ImageView imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.path;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(path))) {
                bitmapDownloaderTask.cancel(true);
            } else {
                // The same path is already being downloaded.
                return false;
            }
        }
        return true;
    }

    private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        // Get downloader task from image view drawable reference
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }
        }
        return null;
    }

    class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private String path;
        private final WeakReference<ImageView> imageViewReference;

        BitmapDownloaderTask(ImageView imageView) {
            imageViewReference = new WeakReference<>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            path = params[0];
            int dstWidth = 128;
            if (imageViewReference.get() != null) {
                dstWidth = imageViewReference.get().getMeasuredWidth();
            }

            // Get image size
            BitmapFactory.Options mBitmapOptions = new BitmapFactory.Options();
            mBitmapOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, mBitmapOptions);
            int srcWidth = mBitmapOptions.outWidth;

            // Set resize options for faster decoding
            mBitmapOptions = new BitmapFactory.Options();
            mBitmapOptions.inScaled = true;
            mBitmapOptions.inSampleSize = 4;
            mBitmapOptions.inDensity = srcWidth;
            mBitmapOptions.inTargetDensity = dstWidth * mBitmapOptions.inSampleSize;

            try {
                // Decode image
                Bitmap bitmap = BitmapFactory.decodeFile(path, mBitmapOptions);

                // Crop image -> square at the center of original image
                if (bitmap.getHeight() > bitmap.getWidth()) {
                    // Crop on Y
                    return Bitmap.createBitmap(bitmap, 0,
                            (bitmap.getHeight() - bitmap.getWidth()) / 2,
                            bitmap.getWidth(), bitmap.getWidth());
                } else {
                    // Crop on X
                    return Bitmap.createBitmap(bitmap,
                            (bitmap.getWidth() - bitmap.getHeight()) / 2,
                            0, bitmap.getHeight(), bitmap.getHeight());
                }
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            // Cache bitmap
            if (bitmap != null) {
                synchronized (bitmapCache) {
                    bitmapCache.put(path, bitmap);
                }
            }

            // Set Bitmap
            ImageView imageView = imageViewReference.get();
            BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
            // Change bitmap only if this process is still associated with it
            if (this == bitmapDownloaderTask) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    /**
     * Blank drawable as a reference to downloader task
     */
    static class DownloadedDrawable extends ColorDrawable {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask) {
            super(Color.BLACK);
            bitmapDownloaderTaskReference =
                    new WeakReference<>(bitmapDownloaderTask);
        }

        BitmapDownloaderTask getBitmapDownloaderTask() {
            return bitmapDownloaderTaskReference.get();
        }
    }

    private Bitmap getBitmapFromCache(String path) {
        synchronized (bitmapCache) {
            final Bitmap bitmap = bitmapCache.get(path);
            if (bitmap != null) {
                // Bitmap found in hard cache
                // Move element to first position, so that it is removed last
                bitmapCache.remove(path);
                bitmapCache.put(path, bitmap);
                return bitmap;
            }
        }
        return null;
    }
}
