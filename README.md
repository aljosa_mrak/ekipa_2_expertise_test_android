# Full Stack Software Engineer - Android test

This is my solution for the expertise test for the job application at Ekipa 2. Problem description is provided in [Problem details](Full Stack Software Engineer - Android test.pdf)

# Problem 1: Installed apps

Program lists all installed apps that include 'outfit7' in their package name. This also includes this app.

# Problem 2: Screenshot viewer

Program shows all pictures in **/Pictures/Screenshots** and **/DCIM/Camera**. By clicking a picture user is prompt to send an email with device info.
